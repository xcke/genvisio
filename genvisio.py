#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Copyright (c) 2017 Gabor, Kis-Hegedus
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY AND ALL EXPRESS OR IMPLIED
# WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF TITLE, MERCHANTABILITY, AGAINST INFRINGEMENT, AND FITNESS
# FOR A PARTICULAR PURPOSE.
#

"""Project

Simple utility to generate Visio file based on a Jinja2 templating.

"""

""" Imports """
import argparse
import logging as log
from jinja2 import Environment, FileSystemLoader
import zipfile
import tempfile
import os
import shutil
import yaml
from zip import zip_functions
import csv
from Methods import template_functions
"""Functions & Methods"""

def get_config(configfile):
    """Pulls YAML configuration from file and returns dict object"""
    with open(configfile) as _:
        return yaml.load(_)

def get_csvconfig(configfile):
    """Loads the element from the CSV file and returns as dict """
    configdict = csv.DictReader(open(configfile), delimiter=';')
    return configdict

def unzip_visio(file, tmp):
    """ This function will unpack/unzip the Visio file
    """
    try:
        zip_ref = zipfile.ZipFile(file, 'r')
        zip_ref.extractall(tmp)
        zip_ref.close()
    except IOError:
        log.info("Error file {} not found!".format(file))
    else:
        log.info("file {} unpacked to {}".format(file, tmp))

def create_tmp():
    """ Will create a temp directory"""
    directory_name = tempfile.mkdtemp()
    log.debug("Created {}".format(directory_name))
    return directory_name

def clean_tmp(temp_dir):
    """ Remove the temp directory"""
    shutil.rmtree(temp_dir)
    log.debug("Tmp directory {} cleaned.".format(temp_dir))

def visio_get_page_files(temp_dir):
    """ This will return the list of the Page[N].xml files"""
    results = []
    list_of_files = os.listdir(temp_dir+"/visio/pages")
    for file in list_of_files:
        if file.find("page") != -1:
            results.append(file)
    log.debug(results)
    return results

def render_visio_xml(tmp, config_dict,csv_config_dict, files):
    """ this is the core function we will generate the XML files and save them back"""
    ENV = Environment(loader=FileSystemLoader(tmp + "\\visio\\pages"))
    ENV.globals['datetimenow'] = template_functions.datetimenow
    ENV.globals['type5_encrypt'] = template_functions.type5_encrypt
    ENV.globals['ipcalc'] = template_functions.netaddr
    ENV.globals['convert_bw'] = template_functions.convert_bw
    ENV.globals['get_address_n'] = template_functions.get_address_n
    # Functions required for rendering template

    for item in files:
        t = ENV.get_template(item)
        gxml = t.render(cfg=config_dict, csv=csv_config_dict)
        the_file = open(tmp+"\\visio\pages\\"+ item, 'w')
        the_file.write(gxml)
        the_file.truncate()
        the_file.close()



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("template_file", help="Name of the template file (Visio .vsdx)")
    parser.add_argument("-c", "--config", help="Name of the configuration file (YAML)", default="config.yaml")
    parser.add_argument("-C", "--csv", help="Name of the configuration file (CSV)", default="config.csv")
    parser.add_argument("-D", "--debug", help="Enable debug mode", action="store_true")
    args = parser.parse_args()
    if args.debug:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        log.info("Verbose output.")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.INFO)

    """ Main program """
    config_dict = get_config(args.config)
    template_file = args.template_file

    """ Load CSV data"""
    csv_data = get_csvconfig(args.csv)

    """ Render new xml files"""
    for row in csv_data:
        """ In this loop we will generate the XML files and pack it. Each iteration will represent a row in the CSV"""
        tmp = create_tmp()
        unzip_visio(template_file, tmp)
        visio_page_xmls = visio_get_page_files(tmp)
        log.debug(row)
        render_visio_xml(tmp, config_dict,row, visio_page_xmls)
        zip_functions.zipdir(tmp, row['NAME']+'.vsdx', False)
        clean_tmp(tmp)

if __name__ == '__main__':
    main()