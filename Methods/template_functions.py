import ipaddress
import datetime
from passlib.hash import md5_crypt

class ipcalc:
    'Class for IP Tools'
    def __init__(self,cidr_input):
        self.cidr = cidr_input
    def netmask(self):
        return str(ipaddress.ip_interface(self.cidr).netmask)
    def host(self):
        return str(ipaddress.ip_interface(self.cidr).ip)
    def network(self):
        return str(ipaddress.ip_interface(self.cidr).network)
    def wildcard(self):
        netmask_int=ipaddress.ip_interface(self.cidr).netmask.packed
        b = BitArray(netmask_int)
        w = ~b
        wildcard_mask = ipaddress.ip_address(w.bytes)
        return str(wildcard_mask)
    def broadcast(self):
        return str(ipaddress.ip_interface(self.cidr).network.broadcast_address)
    def hosts(self):
        return list(ipaddress.ip_network(self.cidr).hosts())
    def __repr__(self):
        return str(self.cidr)

def netaddr(prefix, what):
    cidr = ipcalc(prefix)
    if what == 'netmask':
        return cidr.netmask()
    if what == 'network':
        return cidr.network()
    if what == 'wildcard':
        return cidr.wildcard()
    if what == 'host':
        return cidr.host()
    if what == None:
        return cidr

def get_address_n(prefix, number):
    """ This function will give back the Nth address of a subnet"""
    cidr = ipaddress.IPv4Network(prefix)
    return cidr[number]

def datetimenow():
    now = datetime.datetime.now()
    return str(now.strftime("%Y-%m-%d %H:%M"))

def type5_encrypt(password):
    m = md5_crypt.encrypt(password,  salt_size=4)
    return m

def convert_bw(bits, requested):
    if requested == 'k':
        return int(bits/1000)
    if requested == 'm':
        return int(bits/1000000)
    if requested == 'g':
        return int(bits/1000000000)