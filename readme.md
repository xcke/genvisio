# GenVisio: Generate Microsoft Visio Diagrams

This small Python program will take CSV and YAML data inputs, and generate Microsoft Visio documents based on a template file.

## Installation

One way for the installation is to use [Miniconda](https://conda.io/miniconda.html) distribution, and creatomitteddicated virtual environment. 

```python
conda create -n genvisio python=3.4 jinja2 pyyaml
activate genvisio
pip install passlib
```

Alternativly you can recreate the environment using:

```python
conda env create -f env.yaml
```

## Usage

### Template file

We can use standard Jinja2 syntax in various Text objects of the VSDX Visio file. The script will unzip the template file, and render the Jinja2 template based on the page[N].xml files.

You might reference any CSV variables with the "csv." prefix.

You might reference any YAML variables using the "cfg." prefeix.

Check out the Drawing1.vsdx for examples.

### Data inputs

The script will generate a Visio diagram for each of the CSV rows. The Jinja2 engine will use variables from the YAML file and also from the respective CSV row.

Check the config.yaml and config.csv for examples.

File names will be created based on the {{ csv.NAME }}, in english the NAME variable from the CSV file.

## Jinja2 custom filters

We can easily implement Jinja2 custom filters. Just add your function into Methods/template_functions.py

```python
def datetimenow():
    now = datetime.datetime.now()
    return str(now.strftime("%Y-%m-%d %H:%M"))
```

And register the function in the genvisio.py file:

```python
def render_visio_xml(tmp, config_dict,csv_config_dict, files):    
    ENV.globals['datetimenow'] = template_functions.datetimenow
```
